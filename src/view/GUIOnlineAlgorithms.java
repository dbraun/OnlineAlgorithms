package view;

import control.Algorithms;
import control.OnlineSimulator;
import model.Graph;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Locale;
import java.util.Scanner;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class GUIOnlineAlgorithms {

    public final static GUIOnlineAlgorithms gui = new GUIOnlineAlgorithms();

    public JComboBox comboBox1;
    private JButton createGraphButton;
    private JButton loadGraphButton;
    private JPanel MainPanel;
    public JTextArea textAreaDebug;
    private JTabbedPane tabbedPane1;
    private JTextArea textAreaGraph;
    private JTextArea textAreaoptGraph;
    private JTextArea textAreaonlineGraph;
    private JTextArea textAreaResults;
    private JLabel version;

    // assume Unicode UTF-8 encoding
    private static final String CHARSET_NAME = "UTF-8";

    // assume language = English, country = US for consistency with System.out.
    private static final Locale LOCALE = Locale.US;

    public GUIOnlineAlgorithms() {
        TextAreaOutputStream taOutputStream = new TextAreaOutputStream(textAreaDebug, "");
        System.setOut(new PrintStream(taOutputStream));

        loadGraphButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser(new File(System.getProperty("user.dir") + "/test/" + comboBox1.getSelectedItem()));

                chooser.setFileFilter(new FileFilter() {
                    public boolean accept(File f) {
                        return (f != null) && (f.isDirectory() || f.getName().toLowerCase().endsWith("graph"));
                    }

                    public String getDescription() {
                        return ".graph";
                    }
                });

                chooser.showOpenDialog(null);
                if (chooser.getSelectedFile() == null) return;
                runAlgorithm(chooser.getSelectedFile());
                tabbedPane1.setSelectedIndex(3);
            }
        });

        createGraphButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIGraphGenerator.newGUI();
                tabbedPane1.setSelectedIndex(3);
            }
        });

        comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIGraphGenerator.gui.comboBox1.setSelectedIndex(comboBox1.getSelectedIndex());
            }
        });
    }

    public void runAlgorithm(File file) {
        textAreaDebug.setText("");
        Scanner scanner = null;
        try {
            scanner = new Scanner(file, CHARSET_NAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (scanner != null) {
            scanner.useLocale(LOCALE);
        }

        OnlineSimulator.graph = new Graph(scanner);
        try {
            OnlineSimulator.graph.setL(OnlineSimulator.getL(OnlineSimulator.graph));
        }catch(IllegalArgumentException e){System.out.println("Graph is not bipartite.");}

        switch (comboBox1.getSelectedIndex()) {
            case 0:
                OnlineSimulator.onlineMatching();
                break;
            case 1:
                OnlineSimulator.onlineFlow();
                break;
            case 2:
                OnlineSimulator.onlineBipartiteMatching();
                break;
            case 3:
                OnlineSimulator.machineScheduling();
                break;
        }
        textAreaGraph.setText(OnlineSimulator.graph.toString());
        textAreaoptGraph.setText(OnlineSimulator.optGraph.toString());
        textAreaonlineGraph.setText(OnlineSimulator.solGraph.toString());
        textAreaResults.setText(OnlineSimulator.result);
            Frame[] frames = JFrame.getFrames();
            for(int i=2;i<frames.length;i++)
                frames[i].dispose();

        try {
            OnlineSimulator.optGraph.display();
            JFrame frame = ((JFrame) JFrame.getFrames()[JFrame.getFrames().length - 1]);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 2, 0);
            frame.setSize(Toolkit.getDefaultToolkit().getScreenSize().width / 2, Toolkit.getDefaultToolkit().getScreenSize().height / 2 - 25);
            frame.setTitle("Optimal Solution Graph");

            OnlineSimulator.solGraph.display();
            frame = ((JFrame) JFrame.getFrames()[JFrame.getFrames().length - 1]);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 2, Toolkit.getDefaultToolkit().getScreenSize().height / 2 - 25);
            frame.setSize(Toolkit.getDefaultToolkit().getScreenSize().width / 2, Toolkit.getDefaultToolkit().getScreenSize().height / 2 - 25);
            frame.setTitle("Online Solution Graph");
        }catch( java.lang.ClassCastException e){}
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Online Algorithms");
        gui.version.setText("Build "+OnlineSimulator.BUILD_NUMBER+" ");
        frame.setContentPane(gui.MainPanel);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocation(0, 0);
        frame.setSize(Toolkit.getDefaultToolkit().getScreenSize().width / 2,Toolkit.getDefaultToolkit().getScreenSize().height-50);
    }

    public class TextAreaOutputStream extends OutputStream {
        private final JTextArea textArea;
        private final StringBuilder sb = new StringBuilder();
        private String title;

        public TextAreaOutputStream(final JTextArea textArea, String title) {
            this.textArea = textArea;
            this.title = title;
            sb.append(title);
        }

        public void flush() {
        }

        public void close() {
        }

        public void write(int b) throws IOException {
            if (b == '\r')
                return;
            if (b == '\n') {
                final String text = sb.toString() + "\n";
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        textArea.append(text);
                    }
                });
                sb.setLength(0);
                sb.append(title);
                return;
            }
            sb.append((char) b);
        }
    }
}
package view;

import control.GraphGenerator;
import control.Statistic;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.util.Random;

import static javax.swing.JFrame.*;

public class GUIGraphGenerator {
    public final static JFrame frame = new JFrame("Graphgenerator");
    public final static GUIGraphGenerator gui = new GUIGraphGenerator();

    public JComboBox comboBox1;
    private JTextField textFieldVL;
    private JTextField textFieldVR;
    private JPanel MainPanel;
    private JButton saveButton;
    private JTextField textFieldV;
    private JTextField textFieldE;
    private JTextField textFieldweightmin;
    private JTextField textFieldweightmax;
    private JLabel L;
    private JLabel R;
    private JLabel weight;
    private JLabel min;
    private JLabel max;
    private JSlider slider1;
    private JSlider slider2;
    private JButton createStatisticsButton;
    private JSlider slider3;
    private JLabel textFieldDistribution;

    private void updateGUI() {
        switch (comboBox1.getSelectedIndex()) {
            case 0: // online b Matching
                setBipartiteVisible(true);
                setWeightVisible(false);
                break;
            case 1: // online flow
                setBipartiteVisible(false);
                setWeightVisible(true);
                break;
            case 2: // bipartite matching
                setBipartiteVisible(true);
                setWeightVisible(true);
                break;
            case 3: // machine scheduling
                setBipartiteVisible(true);
                setWeightVisible(true);
                break;
        }
    }

    private void setBipartiteVisible(boolean bol) {
        textFieldVL.setVisible(bol);
        textFieldVR.setVisible(bol);
        L.setVisible(bol);
        R.setVisible(bol);
        slider1.setVisible(bol);
        textFieldDistribution.setVisible(bol);
        slider3.setVisible(bol);

        saveButton.setEnabled(bol);
        createStatisticsButton.setEnabled(bol);
    }

    private void setWeightVisible(boolean bol) {
        weight.setVisible(bol);
        min.setVisible(bol);
        max.setVisible(bol);
        textFieldweightmax.setVisible(bol);
        textFieldweightmin.setVisible(bol);
    }

    public GUIGraphGenerator() {
        comboBox1.setSelectedIndex(GUIOnlineAlgorithms.gui.comboBox1.getSelectedIndex());
        updateGUI();
        comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateGUI();
            }
        });
        saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIOnlineAlgorithms.gui.textAreaDebug.setText("");
                JFileChooser chooser = new JFileChooser(new File(System.getProperty("user.dir") + "/test/" + comboBox1.getSelectedItem()));

                chooser.setFileFilter(new FileFilter() {
                    public boolean accept(File f) {
                        return (f != null) && (f.isDirectory() || f.getName().toLowerCase().endsWith("graph"));
                    }

                    public String getDescription() {
                        return ".graph";
                    }
                });

                Random random = new Random();
                chooser.setSelectedFile(new File("test" + random.nextInt(10000)));
                if (chooser.showSaveDialog(null) == 1 || chooser.getSelectedFile() == null) return;
                GraphGenerator.generateAndSaveGraph(chooser.getSelectedFile().getAbsolutePath(), comboBox1.getSelectedIndex(), Integer.parseInt(textFieldV.getText()), Integer.parseInt(textFieldVL.getText()), Integer.parseInt(textFieldVR.getText()), Integer.parseInt(textFieldE.getText()), Double.parseDouble(textFieldweightmin.getText()), Double.parseDouble(textFieldweightmax.getText()),(double)(slider3.getValue())/100);
                String file = chooser.getSelectedFile().toString();
                if (!file.endsWith(".graph")) file += ".graph";
                    GUIOnlineAlgorithms.gui.runAlgorithm(new File(file));
            }
        });
        createStatisticsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIOnlineAlgorithms.gui.textAreaDebug.setText("");
                JFileChooser chooser = new JFileChooser(new File(System.getProperty("user.dir")+"/test/"+comboBox1.getSelectedItem()));

                chooser.setFileFilter(new FileFilter() {
                    public boolean accept(File f) {
                        return !(f == null || f.isDirectory()) && f.getName().toLowerCase().endsWith("csv");
                    }

                    public String getDescription() {
                        return ".csv";
                    }
                });
                chooser.setSelectedFile(new File("Statistik" + new Random().nextInt(10000)));
                if (chooser.showSaveDialog(null) == 1 || chooser.getSelectedFile() == null) return;
                String file = chooser.getSelectedFile().toString();
                if (!file.endsWith(".csv")) file += ".csv";
                Statistic.exportStatistics(new File(file), comboBox1.getSelectedIndex(), Integer.parseInt(textFieldV.getText()), Integer.parseInt(textFieldVL.getText()), Integer.parseInt(textFieldVR.getText()), Integer.parseInt(textFieldE.getText()), Double.parseDouble(textFieldweightmin.getText()), Double.parseDouble(textFieldweightmax.getText()),(double)(slider3.getValue())/100);
            }
        });
        textFieldV.addCaretListener(new CaretListener() {
            @Override
            public void caretUpdate(CaretEvent e) {
                try {
                    if (slider1.getMaximum() == Integer.parseInt(textFieldV.getText())) return;
                    slider1.setMaximum(Integer.parseInt(textFieldV.getText()) - 1);
                    slider1.setValue(Integer.parseInt(textFieldV.getText()) / 2);
                    if (comboBox1.getSelectedIndex() == 1)
                        slider2.setMaximum(Integer.parseInt(textFieldV.getText()) * (Integer.parseInt(textFieldV.getText()) - 1) / 2);
                } catch (Exception e1) { e1.printStackTrace();
                }
            }
        });
        textFieldVL.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                slider1.setValue(Integer.parseInt(textFieldVL.getText()));
                slider2.setMinimum(slider1.getValue());
            }
        });
        textFieldVR.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                slider1.setValue(Integer.parseInt(textFieldV.getText()) - Integer.parseInt(textFieldVR.getText()));
            }
        });
        comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIOnlineAlgorithms.gui.comboBox1.setSelectedIndex(comboBox1.getSelectedIndex());
                if (comboBox1.getSelectedIndex() == 1)
                    slider2.setMaximum(Integer.parseInt(textFieldV.getText()) * (Integer.parseInt(textFieldV.getText()) - 1) / 2);
                else
                    slider2.setMaximum((Integer.parseInt(textFieldV.getText()) - slider1.getValue()) * slider1.getValue());
            }
        });
        textFieldweightmin.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                textFieldweightmin.setText(textFieldweightmin.getText().replaceAll(",", "."));
            }
        });
        textFieldweightmax.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                textFieldweightmax.setText(textFieldweightmax.getText().replaceAll(",", "."));
            }
        });
        slider1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                textFieldVL.setText(slider1.getValue() + "");
                textFieldVR.setText(Integer.parseInt(textFieldV.getText()) - slider1.getValue() + "");
                if (comboBox1.getSelectedIndex() != 1) {
                    slider2.setMaximum((Integer.parseInt(textFieldV.getText()) - slider1.getValue()) * slider1.getValue());
                    slider2.setMinimum(slider1.getValue());
                }
            }
        });
        slider2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                textFieldE.setText(slider2.getValue() + "");
            }
        });
        textFieldE.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                slider2.setValue((Integer.parseInt(textFieldE.getText())));
            }
        });
    }

    public static void newGUI() {
        frame.setContentPane(gui.MainPanel);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 2, 0);
        frame.setSize(Toolkit.getDefaultToolkit().getScreenSize().width / 2,Toolkit.getDefaultToolkit().getScreenSize().height-50);
    }

    public static void main(String[] args) {
        GUIGraphGenerator gui = new GUIGraphGenerator();
        frame.setContentPane(gui.MainPanel);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocation(0, 0);
        frame.setSize(Toolkit.getDefaultToolkit().getScreenSize().width,Toolkit.getDefaultToolkit().getScreenSize().height-50);
    }
}
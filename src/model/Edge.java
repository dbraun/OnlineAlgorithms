package model;

/**
 * The <tt>Edge</tt> class represents a weighted direected edge in an
 * {@link Graph}. Each edge consists of two integers
 * (naming the two vertices) and a real-value weight. The data type
 * provides methods for accessing the two endpoints of the directed edge and
 * the weight.
 */

public class Edge implements Comparable<Edge> {
    private static final int INFINITY = Integer.MAX_VALUE;
    private final int v;
    private final int w;
    private final double weight;
    private long capacity;

    /**
     * Initializes a directed edge from vertex <tt>v</tt> to vertex <tt>w</tt> with
     * the given <tt>weight</tt>.
     *
     * @param v      the tail vertex
     * @param w      the head vertex
     * @param weight the weight of the directed edge
     * @throws IndexOutOfBoundsException if either <tt>v</tt> or <tt>w</tt>
     *                                   is a negative integer
     * @throws IllegalArgumentException  if <tt>weight</tt> is <tt>NaN</tt>
     */
    public Edge(int v, int w, double weight) {
        if (v < 0) throw new IndexOutOfBoundsException("Vertex names must be nonnegative integers");
        if (w < 0) throw new IndexOutOfBoundsException("Vertex names must be nonnegative integers");
        if (Double.isNaN(weight)) throw new IllegalArgumentException("Weight is NaN");
        this.v = v;
        this.w = w;
        this.weight = weight;
        this.capacity = 1;
    }

    /**
     * Initializes a directed edge from vertex <tt>v</tt> to vertex <tt>w</tt> with
     * the given <tt>weight</tt> and <tt>capacity</tt>.
     *
     * @param v        the tail vertex
     * @param w        the head vertex
     * @param weight   the weight of the directed edge
     * @param capacity the capacity of the directed edge
     * @throws IndexOutOfBoundsException if either <tt>v</tt> or <tt>w</tt>
     *                                   is a negative integer
     * @throws IllegalArgumentException  if <tt>weight</tt> is <tt>NaN</tt>
     */
    public Edge(int v, int w, double weight, long capacity) {
        this(v, w, weight);
        this.capacity = capacity;
    }

    /**
     * create a copy of the given <tt>edge</tt>
     *
     * @param e edge which we copy
     */
    public Edge(Edge e) {
        this(e.v, e.w, e.weight(), e.capacity());
    }

    /**
     * Initializes a directed edge from vertex <tt>v</tt> to vertex <tt>w</tt> with
     * <tt>weight</tt> 1.
     *
     * @param v the tail vertex
     * @param w the head vertex
     */
    public Edge(int v, int w) {
        this(v, w, 1);
    }

    /**
     * Returns the tail vertex of the directed edge.
     *
     * @return the tail vertex of the directed edge
     */
    public int from() {
        return v;
    }

    /**
     * Returns the head vertex of the directed edge.
     *
     * @return the head vertex of the directed edge
     */
    public int to() {
        return w;
    }

    /**
     * Returns the weight of the directed edge.
     *
     * @return the weight of the directed edge
     */
    public double weight() {
        return weight;
    }

    /**
     * Returns the capacity of the directed edge.
     *
     * @return the capacity of the directed edge
     */
    public long capacity() {
        return capacity;
    }

    /**
     * Adds the capacity to the directed edge.
     *
     * @param u the capacity, we add to the directed edge
     */
    public void addCapacity(long u) {
        if (this.capacity == INFINITY) return;
        this.capacity = this.capacity + u;
    }

    /**
     * Returns a string representation of this directed edge.
     *
     * @return a string representation of this directed edge
     */
    public String toString() {
        if (weight == 1) return String.format("( %d -> %d ) ", v, w);
        if ((int) weight == weight) return String.format("( %d -> %d ): " + (int) weight, v, w);
        return String.format("( %d -> %d ): "+ weight, v, w);
    }

    /**
     * Compares this directed edge with other directed edge.
     *
     * @param other the other directed edge
     * @return -1 if this lower other, 0 this equals other and 1 if this greater other
     */
    public int compareTo(Edge other) {
        return (int) (this.weight() - other.weight());
    }
}
package model;

/******************************************************************************
 * An edge-weighted undirected graph, implemented using adjacency lists.
 * Parallel edges and self-loops are permitted.
 ******************************************************************************/

import grph.Grph;
import grph.algo.coloring.BipartitenessAlgorithm;
import grph.algo.coloring.GraphColoring;
import grph.in_memory.InMemoryGrph;
import toools.gui.EGA16Palette;
import view.GUIOnlineAlgorithms;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

/**
 *  The <tt>Graph</tt> class represents an edge-weighted
 *  graph of vertices named 0 through <em>V</em> - 1, where each
 *  undirected edge is of type {@link Edge} and has a real-valued weight.
 *  It supports the following two primary operations: add an edge to the graph,
 *  iterate over all of the edges incident to a vertex. It also provides
 *  methods for returning the number of vertices <em>V</em> and the number
 *  of edges <em>E</em>. Parallel edges and self-loops are permitted.
 *  <p>
 *  This implementation uses an adjacency-lists representation, which
 *  is a vertex-indexed array of @link{LinkedList} objects.
 *  All operations take constant time (in the worst case) except
 *  iterating over the edges incident to a given vertex, which takes
 *  time proportional to the number of such edges.
 */
public class Graph {
    public static String NEWLINE = System.getProperty("line.separator");

    private final int V;
    private int L;
    private int E;
    private LinkedList<Edge>[] adj;
    public double[] cost;

    /**
     * Initializes an empty graph with <tt>V</tt> vertices and 0 edges.
     *
     * @param  V the number of vertices
     * @throws IllegalArgumentException if <tt>V</tt> < 0
     */
    public Graph(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
        this.V = V;
        this.E = 0;
        adj = (LinkedList<Edge>[]) new LinkedList[V];
        for (int v = 0; v < V; v++) adj[v] = new LinkedList<>();
        cost = new double[V];
    }

    /**
     * Initializes a random edge-weighted graph with <tt>V</tt> vertices and <em>E</em> edges.
     *
     * @param  V the number of vertices
     * @param  E the number of edges
     * @throws IllegalArgumentException if <tt>V</tt> < 0
     * @throws IllegalArgumentException if <tt>E</tt> < 0
     */
    public Graph(int V, int E) {
        this(V);
        if (E < 0) throw new IllegalArgumentException("Number of edges must be nonnegative");
        Random random = new Random();
        for (int i = 0; i < E; i++) {
            int v = random.nextInt(V);
            int w = random.nextInt(V);
            //    double weight = Math.round(100 * random.nextDouble()) / 100.0;
            Edge e = new Edge(v, w, 1);
            addEdge(e);
        }
    }

    /**
     * Initializes a random edge-weighted graph with <tt>V1+V2</tt> vertices and <em>E</em> edges
     * from <tt>V1</tt> to <tt>V2</tt> with capacity of 1.
     *
     * @param V1 the number of vertices on the left side
     * @param V2 the number of vertices on the right side
     * @param E the number of edges from V1 to V2
     */
    public Graph(int V1, int V2, int E) {
        this(V1 + V2);
        L = V1;
        Random random = new Random();
        for (int i = 0; i < E; i++) {
            int v = random.nextInt(V1);
            int w = random.nextInt(V2) + V1;
            Edge e = new Edge(v, w, 1);
            addEdge(e);
        }
    }

    /**
     * Initializes an edge-weighted graph from an input stream.
     * The format is the number of vertices <em>V</em>,
     * followed by the number of edges <em>E</em>,
     * followed by <em>E</em> pairs of vertices and edge weights,
     * with each entry separated by whitespace.
     *
     * @param  scanner the input stream
     * @throws IndexOutOfBoundsException if the endpoints of any edge are not in prescribed range
     * @throws IllegalArgumentException if the number of vertices or edges is negative
     */
    public Graph(Scanner scanner) {
        this(scanner.nextInt());

        System.out.print("loading graph: ");

        scanner.next();
        int E = scanner.nextInt();
        scanner.next();
        if (E < 0) throw new IllegalArgumentException("Number of edges must be nonnegative");
        try{
            L = scanner.nextInt();
            scanner.nextLine();
        } catch (Exception e1){}

        while (scanner.hasNext()) {
            scanner.next();
            int v = scanner.nextInt();
            scanner.next();
            int w = scanner.nextInt();
            scanner.next();
            double weight = 1;
            try {
                weight = scanner.nextDouble();
            } catch (Exception e1) {
            }
            if (cost[v] > 0 && cost[v] != weight)
                cost[v] = -1;
            else if (cost[v] == 0)
                cost[v] = weight;
            Edge e = new Edge(v, w, weight);
            addEdge(e);
        }
        System.out.println("Successful");
    }

    /**
     * Initializes a new edge-weighted graph that is a deep copy of <tt>G</tt>.
     *
     * @param  G the edge-weighted graph to copy
     */
    public Graph(Graph G) {
        this(G.V());
        this.E = G.E();
        for (int v = 0; v < G.V(); v++) {
            // reverse so that adjacency list is in same order as original
            Stack<Edge> reverse = new Stack<>();
            for (Edge e : G.adj[v]) {
                reverse.push(e);
            }
            for (Edge e : reverse) {
                adj[v].add(new Edge(e));
            }
        }
        this.setL(G.L());
        System.arraycopy(G.cost,0,this.cost,0,G.cost.length);
    }

    /**
     * Returns a new Graph with reverse edges.
     *
     * @return Graph with reverse edges of this graph
     */
    public Graph reverse(){
        Graph G = new Graph(this.V());
        G.setL(this.L());
        for (Edge e : this.edges())
            G.addDirectedEdge(new Edge(e.to(),e.from(),e.weight(),e.capacity()));
        System.arraycopy(this.cost,0,G.cost,0,this.cost.length);
        return G;
    }

    /**
     * Returns the number of vertices in this edge-weighted graph.
     *
     * @return the number of vertices in this edge-weighted graph
     */
    public int V() {
        return V;
    }

    /**
     * Returns the number of edges in this edge-weighted graph.
     *
     * @return the number of edges in this edge-weighted graph
     */
    public int E() {
        return E;
    }

    /**
     * Throws IndexOutOfBoundException if vertex <tt>v</tt> is not in the set.
     *
     * @param v vertex which we want to validate
     */
    private void validateVertex(int v) {
        if (v < 0 || v >= V)
            throw new IndexOutOfBoundsException("vertex " + v + " is not between 0 and " + (V - 1));
    }

    /**
     * Adds the undirected edge <tt>e</tt> to this edge-weighted graph.
     *
     * @param  e the edge
     * @throws IndexOutOfBoundsException unless both endpoints are between 0 and V-1
     */
    public void addEdge(Edge e) {
        int v = e.from();
        int w = e.to();
        validateVertex(v);
        validateVertex(w);
        if (!existEdge(e.from(), e.to()) && !existEdge(e.to(), e.from())) E++;
        if (existEdge(v, w))
            getEdge(v, w).addCapacity(1);
        else
            adj[v].add(e);
    }

    /**
     * Adds the directed edge <tt>e</tt> to this edge-weighted graph.
     *
     * @param e the edge
     * @throws IndexOutOfBoundsException unless both endpoints are between 0 and V-1
     */
    public void addDirectedEdge(Edge e) {
        int v = e.from();
        int w = e.to();
        validateVertex(v);
        validateVertex(w);
        if (!existEdge(e.from(), e.to()) && !existEdge(e.to(), e.from())) E++;
        if (existEdge(v, w)) {
            getEdge(v, w).addCapacity(1);
        } else {
            adj[v].add(e);
        }
    }

    /**
     * Deletes the edge <tt>e</tt> from the edge-weighted graph.
     *
     * @param e the edge which we delete
     */
    public void deleteDirectedEdge(Edge e) {
        adj[e.from()].remove(e);
        if (!existEdge(e.from(), e.to()) && !existEdge(e.to(), e.from())) E--;
    }

    /**
     * Returns true if an edge <tt>(v,w)</tt> exists in the edge-weighted graph.
     *
     * @param v the head of the edge
     * @param w the tail of the edge
     * @return true of an edge exists
     */
    public boolean existEdge(int v, int w) {
        return getEdge(v, w) != null;
    }

    /**
     * Returns the edge <tt>e</tt> which represent <tt>(v,w)</tt>.
     *
     * @param v the head of the edge
     * @param w the tail of the edge
     * @return returns the edge <tt>(v,w)</tt>
     * @throws IllegalArgumentException if edge not exists
     */
    public Edge getEdge(int v, int w) {
        for (Edge x : adj(v)) {
            if (x.to() == w) return x;
        }
        return null;
    }

    /**
     * Flips the directed-weighted edge.
     *
     * @param e the edge
     */
    public void flipEdge(Edge e) {
        if (e.capacity() <= 1) {
            deleteDirectedEdge(e);
        } else {
            e.addCapacity(-1);
        }
        addDirectedEdge(new Edge(e.to(), e.from(), e.weight()));
    }

    /**
     * Returns the number of the vertices on the left side if the graph is bipartite.
     *
     * @return returns the number of the vertices on the left side if the graph is bipartite
     */
    public int L() {
        return L;
    }

    /**
     * Sets the number of vertices on the left side to L.
     *
     * @param L the number of the vertices on the left side
     */
    public void setL(int L) {
        this.L = L;
    }

    /**
     * Returns the edges incident on vertex <tt>v</tt>.
     *
     * @param  v the vertex
     * @return the edges incident on vertex <tt>v</tt> as an Iterable
     * @throws IndexOutOfBoundsException unless 0 <= v < V
     */
    public Iterable<Edge> adj(int v) {
        validateVertex(v);
        return adj[v];
    }

    /**
     * Returns the degree of vertex <tt>v</tt>.
     *
     * @param  v the vertex
     * @return the degree of vertex <tt>v</tt>
     * @throws IndexOutOfBoundsException unless 0 <= v < V
     */
    public int degree(int v) {
        validateVertex(v);
        return adj[v].size();
    }

    /**
     * Returns the makespan of <tt>v</tt>.
     *
     * @param v the vertex
     * @return the makespan of vertex <tt>v</tt>
     */
    public double makespan(int v){
        validateVertex(v);
        double cost=0;
        for(Edge e: adj(v)) cost+=e.weight();
        return cost;
    }

    /**
     * Returns all edges in this edge-weighted graph.
     * To iterate over the edges in this edge-weighted graph, use foreach notation:
     * <tt>for (Edge e : G.edges())</tt>.
     *
     * @return all edges in this edge-weighted graph, as an iterable
     */
    public Iterable<Edge> edges() {
        LinkedList<Edge> list = new LinkedList<>();
        for (int v = 0; v < V; v++) {
            for (Edge e : adj(v)) {
                list.add(e);
            }
        }
        return list;
    }

    /**
     * Returns a string representation of the edge-weighted graph.
     * This method takes time proportional to <em>E</em> + <em>V</em>.
     *
     * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
     *         followed by the <em>V</em> adjacency lists of edges
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(V).append(" vertices, ").append(E).append(" edges");
        for (int v = 0; v < V; v++) {
            if (this.degree(v) == 0) continue;
            s.append(NEWLINE);
            for (Edge e : adj[v]) {
                s.append(e).append(" ");
            }
            s.deleteCharAt(s.length() - 1);
        }
        return s.toString();
    }

    /**
     * open a window that display the  visually
     */
    public void display() {
        if(E()>500 || V() >500){
            System.out.println("This graph is to big to draw.");
            return;
        }

        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        Grph g = new InMemoryGrph();
        g.addNVertices(this.V());

        // coloring edges
        for (Edge e : this.edges()) {
            int edge = g.addSimpleEdge(e.from(), e.to(),GUIOnlineAlgorithms.gui.comboBox1.getSelectedIndex()== 1);
            g.getEdgeLabelProperty().setValue(edge,e.weight()+"");
            if ((GUIOnlineAlgorithms.gui.comboBox1.getSelectedIndex()!= 1 && e.from() >= this.L()) || (GUIOnlineAlgorithms.gui.comboBox1.getSelectedIndex()== 1 &&e.from() < e.to()))
                g.getEdgeColorProperty().setValue(edge, 0);
        }

        // coloring vertices of set L
        for (int i=0;i<=L();i++)
            g.getVertexColorProperty().setValue(i, 7);

        // coloring source if we have a flow
        if (GUIOnlineAlgorithms.gui.comboBox1.getSelectedIndex() == 1)
            g.getVertexColorProperty().setValue(V() - 1, 8);

        // remove vertices which has no edges
        g.removeVertices(g.getVerticesOfDegree(0));

        // display graph
        g.display();
    }
}
package control;

/******************************************************************************
 * Run breadth first search on a Graph.
 * Runs in O(E + V) time.
 ******************************************************************************/

import model.Edge;
import model.Graph;

import java.util.PriorityQueue;
import java.util.Stack;

/**
 * The <tt>BreadthFirstSearch</tt> class represents a data type for finding
 * shortest paths (weights of edges) from a s vertex <em>s</em>
 * to every other vertex in the Graph.
 * <p/>
 * This implementation uses breadth-first search.
 * The constructor takes time proportional to <em>V</em> + <em>E</em>,
 * where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 * It uses extra space (not including the Graph) proportional to <em>V</em>.
 */
public class BreadthFirstSearch {
    private static final int INFINITY = Integer.MAX_VALUE;
    private boolean[] marked;  // marked[v] = is there an s->v path?
    private int[] edgeTo;      // edgeTo[v] = last edge on shortest s->v path
    private double[] distTo;   // distTo[v] = length of shortest s->v path
    private int s;             // source vertex

    /**
     * Computes the shortest path from <tt>s</tt> and every other vertex in graph <tt>G</tt>.
     *
     * @param G the Graph
     * @param s the s vertex
     */
    public BreadthFirstSearch(Graph G, int s) {
        this.s = s;
        marked = new boolean[G.V()];
        distTo = new double[G.V()];
        edgeTo = new int[G.V()];
        for (int v = 0; v < G.V(); v++)
            distTo[v] = INFINITY;
        bfs(G, s);
    }

    // BFS from single s
    private void bfs(Graph G, int s) {
        PriorityQueue<Edge> q = new PriorityQueue<>();
        marked[s] = true;
        distTo[s] = 0;

        for (Edge w : G.adj(s)) {
            q.offer(new Edge(w.from(), w.to(), w.weight()));
        }

        while (!q.isEmpty()) {
            Edge e = q.poll();
            if (distTo(e.to())>e.weight()){
                edgeTo[e.to()] = e.from();
                distTo[e.to()] = e.weight();
                marked[e.to()] = true;
                for (Edge w : G.adj(e.to())) {
                    q.offer(new Edge(w.from(), w.to(), distTo[w.from()] + w.weight()));
                }
            }
        }
    }

    /**
     * Is there a directed path from the s <tt>s</tt> to vertex <tt>v</tt>?
     *
     * @param v the vertex
     * @return <tt>true</tt> if there is a directed path, <tt>false</tt> otherwise
     */
    public boolean hasPathTo(int v) {
        return marked[v];
    }

    /**
     * Returns the number of edges in a shortest path from the s <tt>s</tt>
     * to vertex <tt>v</tt>?
     *
     * @param v the vertex
     * @return the number of edges in a shortest path
     */
    public double distTo(int v) {
        return distTo[v];
    }

    /**
     * Returns a shortest path from <tt>s</tt> to <tt>v</tt>, or
     * <tt>null</tt> if no such path exist.
     *
     * @param v the vertex
     * @return the sequence of vertices on a shortest path, as an Iterable
     */
    public Iterable<Integer> pathTo(int v) {
        if (!hasPathTo(v)) return null;
        Stack<Integer> path = new Stack<>();
        int x;
        for (x = v; x != s; x = edgeTo[x]) {
            path.add(0, x);
        }
        path.add(0, x);
        return path;
    }
}
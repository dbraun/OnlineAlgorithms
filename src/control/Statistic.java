package control;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Statistic {
    public static void exportStatistics(File file, int graphtype, int V, int V1, int V2, int E, double weight_min, double weight_max, double distribution) {
        PrintWriter load_out = null;
        try {
            load_out = new PrintWriter(new FileOutputStream(file.getAbsolutePath().replaceFirst(".csv","Load.csv")), true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintWriter reassignment_out = null;
        try {
            reassignment_out = new PrintWriter(new FileOutputStream(file.getAbsolutePath().replaceFirst(".csv","Reassignments.csv")), true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (load_out != null) {
            load_out.println("Instance;online Load;optimal Load");
        }
        if (reassignment_out != null) {
            reassignment_out.println("Instance;online Reassignments;online Reassignments Optimal");
        }
        // 100 graphs
        for (int i = 0; i < 100; i++) {
            OnlineSimulator.graph = GraphGenerator.generateGraph(graphtype, V, V1, V2, E, weight_min, weight_max,distribution);
            switch (graphtype) {
                case 0:
                    OnlineSimulator.onlineMatching();
                    break;
                case 1:
                    OnlineSimulator.onlineFlow();
                    break;
                case 2:
                    OnlineSimulator.onlineBipartiteMatching();
                    break;
                case 3:
                    OnlineSimulator.machineScheduling();
                    break;
            }
            if (load_out != null) {
                load_out.println(i+";"+OnlineSimulator.K_online+";"+OnlineSimulator.K);
            }
            if (reassignment_out != null) {
                reassignment_out.println(i+";"+OnlineSimulator.total_costs+";"+OnlineSimulator.total_costs_opt);
            }
        }
        if (load_out != null) {
            load_out.close();
        }
        if (reassignment_out != null) {
            reassignment_out.close();
        }
    }
}
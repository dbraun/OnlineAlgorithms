package control;

import model.Edge;
import model.Graph;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Random;

/**
 *  The <tt>GraphGenerator</tt> class provides static methods for creating random bipartite graphs.
 */
public class GraphGenerator {

    /**
     * Returns a random simple bipartite graph on <tt>V1</tt> and <tt>V2</tt> vertices
     * with <tt>E</tt> edges.
     * @param V1 the number of vertices in one partition
     * @param V2 the number of vertices in the other partition
     * @param E the number of edges
     * @return a random simple bipartite graph on <tt>V1</tt> and <tt>V2</tt> vertices,
     *    containing a total of <tt>E</tt> edges
     * @throws IllegalArgumentException if no such simple bipartite graph exists
     */
    public static Graph bipartite(int V1, int V2, int E,double weight_min, double weight_max, boolean weight, double distribution) {
        if (E > (long) V1*V2) throw new IllegalArgumentException("Too many edges");
        if (E < V1) throw new IllegalArgumentException("Too few edges");
        Graph G = new Graph(V1, V2,0);
        Random random = new Random();
        LinkedList<Edge> edges = new LinkedList<>();
        for (int i=0;i<V1;i++){
            int j = V1 + random.nextInt(V2);
            G.addEdge(new Edge(i,j, Math.round((weight_min + random.nextDouble() * (weight_max - weight_min))*100.0)/100.0));
            if(weight)
                G.cost[i] = G.getEdge(i,j).weight();
            for(int k=V1;k<V1+V2;k++){
                if(k==j) continue;
                edges.addLast(new Edge(i, k));
            }
        }
        while (G.E() < E) {
            int lowerBound= 0;
            int upperBound= edges.size();
            while (lowerBound+1<upperBound){
                if(random.nextDouble()<distribution)
                    upperBound = upperBound - (upperBound-lowerBound)/2;
                else
                    lowerBound = lowerBound + (upperBound-lowerBound)/2;
            }
            Edge k = edges.remove(lowerBound);
            int i = k.from();
            int j = k.to();

            double edge_weight = Math.round((weight_min + random.nextDouble() * (weight_max - weight_min))*100.0)/100.0;
            if (weight)
               edge_weight = G.cost[i];
            Edge e = new Edge(i,j, edge_weight);
            G.addEdge(e);
        }
        return G;
    }

    /**
     * generate graph and returns
     * @param graphtype type of this graph
     * @param V cardinality of vertices
     * @param V1 cardinality of left side vertices
     * @param V2 cardinality of right side vertices
     * @param E cardinality of edges
     * @param weight_min weight minimum of vertices
     * @param weight_max weight maximum of vertices
     * @param distribution distribution of cardinality from vertices that arrive online
     * @return Graph our graph
     */
    public static Graph generateGraph(int graphtype, int V, int V1, int V2, int E,double weight_min, double weight_max, double distribution){
        Graph G = null;
        System.out.print("creating graph: ");

        switch (graphtype){
            case 0: // online b Matching
                G = bipartite(V1,V2,E,1,1,false,distribution);
                break;
            case 1: // online flow
                break;
            case 2: // bipartite matching
                G = bipartite(V1,V2,E,weight_min,weight_max,true,distribution);
                break;
            case 3: // machine scheduling
                G = bipartite(V1, V2, E, weight_min, weight_max,true,distribution);
                break;
        }
        System.out.println("Successful");
        return G;
    }

    /**
     * Generate graph and save it in a graph-file
     * @param file name of the file
     * @param graphtype type of this graph
     * @param V cardinality of vertices
     * @param V1 cardinality of left side
     * @param V2 cardinality of right side
     * @param E cardinality of edges
     * @param weight_min weight minimum of an edge
     * @param weight_max weight maximum of an edge
     * @param distribution distribution of edges from vertices that arrives online
     */
    public static void generateAndSaveGraph(String file, int graphtype, int V, int V1, int V2, int E,double weight_min, double weight_max, double distribution){
        Graph G = generateGraph(graphtype,V,V1,V2,E,weight_min,weight_max,distribution);
        saveGraph(G,file);
    }

    /**
     * save this graph in a graph file
     * @param G graph we want to save
     * @param file name of this file
     */
    public static void saveGraph(Graph G, String file){
        System.out.print("saving graph: ");
        if (!file.endsWith(".graph")) file += ".graph";
        // write to file
        PrintWriter out = null;
        try {
            out  = new PrintWriter(new FileOutputStream(file), true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (out != null) {
            out.print(G);
            out.flush();
            out.close();
        }
        System.out.println("Successful");
        System.out.println("Path: "+ file);
    }
}
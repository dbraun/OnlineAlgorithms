package control;

import model.Edge;
import model.Graph;
import org.miv.graphstream.algorithm.Algorithm;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class OnlineSimulator {
    public static final String BUILD_NUMBER = "474";

    public static String result="";

    public static double total_costs = 0;
    public static double total_costs_opt = 0;

    public static Graph graph;
    public static Graph optGraph;
    public static Graph solGraph;

    public static double K;
    public static double K_online;
    public static double K_online_opt;

    public static double gamma;

    /**
     * G = (L,R) cost
     */
    public static void onlineBipartiteMatching(){
        optGraph = new Graph(graph.L(),graph.V()-graph.L(),0);
        for (int i=0;i<optGraph.L();i++) {
            Algorithms.offlineMatchingAlgorithm(optGraph, i, graph.adj(i));
        }
        K = matchingLoad(optGraph);
        total_costs_opt = flowCosts(optGraph);

        result="optimal offline Load is " + (int)K + System.getProperty("line.separator");
        result+="with flow costs: " + total_costs_opt+ System.getProperty("line.separator");
        result+="----------------------------------------------" + System.getProperty("line.separator");

        for (int j=0;j<3;j+=2) {
            total_costs = 0;
            // create L u R + source
            Graph G2 = new Graph(graph.L(), graph.V() - graph.L() + 1, 0);

            int e = 1;
            // add edge (s,v) with capacity K and v in R
            for (int v = G2.L(); v < G2.V() - 1; v++) {
                G2.addDirectedEdge(new Edge(G2.V() - 1, v, 0, (j + e) * (int)K));
            }

            for (int v = 0; v < G2.L(); v++) {
                // add edges of sink v in L as (*,v) with capacity infinity
                for (Edge w : graph.adj(v)) {
                    if (w.from() != v)
                        throw new IllegalArgumentException("format in G is corrupt: Edge " + w + " in vertex " + v);
                    G2.addDirectedEdge(new Edge(w.to(), w.from(), w.weight(), Integer.MAX_VALUE));
                }
                // calculate flow for sink v
                Algorithms.onlineFlowAlgorithm(G2, v);
            }

            // create graph without the helper edges
            solGraph = new Graph(graph.L(), graph.V() - graph.L(), 0);
            for (int i = 0; i < graph.V(); i++) {
                for (Edge w : G2.adj(i)) {
                    if (w.to()>= graph.V() || solGraph.existEdge(w.from(),w.to())) continue;
                    solGraph.addDirectedEdge(new Edge(w.to(), w.from(), w.weight()));
                }
            }

            K_online = matchingLoad(solGraph);
            if(j== 0) {
                result += "online optimal Load " + (int) K_online + System.getProperty("line.separator");
                result += "need total augmentation flow costs of " + total_costs + System.getProperty("line.separator");
                result+="----------------------------------------------" + System.getProperty("line.separator");
                total_costs_opt = total_costs;
            }else{
                result += "our online Load is " + (int) K_online + System.getProperty("line.separator");
                result += " with total augmentation flow costs of " + total_costs + System.getProperty("line.separator");
            }
        }
    }

    /**
     * G = (T,V,s)
     */
    public static void onlineFlow(){
        total_costs = 0;

        optGraph = new Graph(graph);

        for (int i=0; i<=graph.L(); i++)
            Algorithms.onlineFlowAlgorithm(optGraph,i);

        // calculate flow cost of optimal solution
        total_costs_opt = 0;
        for (Edge e: optGraph.edges())
            if (e.from() < e.to())
                total_costs_opt += e.capacity()*e.weight();
        result="optimal flow costs "+ total_costs_opt + System.getProperty("line.separator");
        result+="online total flow costs without violate costs: "+ total_costs + System.getProperty("line.separator");

        total_costs = 0;
        solGraph = new Graph(graph);
        double epsilon = 1;
        // set capacities to (2+epsilon)
        for (Edge e:solGraph.edges()) {
            long cap = e.capacity();
            e.addCapacity(cap + (long)(epsilon*cap));
        }

        for (int i=0; i<=graph.L(); i++)
            Algorithms.onlineFlowAlgorithm(solGraph,i);

        for (Edge e:solGraph.edges())
            if(solGraph.existEdge(e.to(),e.from()) && e.to()<e.from())
                solGraph.deleteDirectedEdge(e);

        result+="online total flow with violate costs: "+ total_costs + System.getProperty("line.separator");
    }

    /**
     * graph is bipartite with (L, R)
     * edges e = (v1,v2) with cost=1
     */
    public static void onlineMatching(){
        optGraph = new Graph(graph.L(),graph.V()-graph.L(),0);
        for (int i=0;i<optGraph.L();i++) {
            Algorithms.offlineMatchingAlgorithm(optGraph, i, graph.adj(i));
        }

        K = matchingLoad(optGraph);
        result="optimal offline Load is "+ (int)K + System.getProperty("line.separator");
        result+="----------------------------------------------" + System.getProperty("line.separator");

        for (int j=1; j<3;j++) {
            total_costs = 0;
            solGraph = new Graph(graph.L(), graph.V() - graph.L(), 0);
            for (int i = 0; i < solGraph.L(); i++) {
                Algorithms.onlineMatchingAlgorithm(solGraph, i, graph.adj(i), j * (int) K);
            }
            K_online = matchingLoad(solGraph);
            if (j == 1) {
                K_online_opt = K_online;
                total_costs_opt = total_costs;
            }
            if (j == 1) {
                result += "online optimal Load " + (int) K_online + System.getProperty("line.separator");
                result += "need " + total_costs + " reassignments" + System.getProperty("line.separator");
                result+="----------------------------------------------" + System.getProperty("line.separator");
            } else {
                result += "our online Load is " + (int) K_online + System.getProperty("line.separator");
                result += "and need " + total_costs + " reassignments" + System.getProperty("line.separator");
            }
        }
    }

    public static void machineScheduling() {
        System.out.println("[m] =" + graph.L());
        optGraph = new Graph(graph.L(),graph.V()-graph.L(),0);
        for(int i=0;i<graph.V();i++) optGraph.cost[i]=graph.cost[i];
        double[] v = sortCosts(graph.cost);
        
        for (int i=0;i<v.length;i++) {
            if(v[i]>=graph.L()) continue;
            Algorithms.offlineMatchingAlgorithm(optGraph, (int) v[i], graph.adj((int) v[i]));
        }

        //for(double cost:graph.cost) total_costs_opt+=cost;
        K = makespan(optGraph);
        result="optimal offline makespan is " + K + System.getProperty("line.separator");
        //result+="total costs are " + total_costs_opt + System.getProperty("line.separator");

        // initial solution Graph
        solGraph = graph.reverse();
        // initial graph for small
        // transform into flow, create L u R + source
        Graph smallGraph = new Graph(graph.L(),graph.V()-graph.L()+1,0);
        // add edge (s,v) with capacity K and v in R
        for (int w = smallGraph.L(); w< smallGraph.V() - 1; w++) {
            smallGraph.addDirectedEdge(new Edge(smallGraph.V() - 1, w, 0, 2*(long)K));
        }
        // initial graphs for big jobs
        Graph[] bigGraph = new Graph[getNumbPot(K)+1];
        for (int i=0;i<bigGraph.length;i++)
            bigGraph[i] = new Graph(graph.L(),graph.V()-graph.L(),0);

        gamma = 9*Math.log(graph.L()*(graph.V()-graph.L()));
        System.out.println("gamma="+gamma);
        System.out.println("K/gamma="+K/gamma);

        double cost = 0;
        //all jobs arrives seq online
        for (int i=0; i<graph.L();i++){
            if (graph.cost[i] < K/gamma)
                //small job, calculate fractional problem and randomize rounding
                Algorithms.smallGraphAlgorithm(smallGraph,i,OnlineSimulator.graph.adj(i));
            else
                //big job, greedy
                Algorithms.bigGraphAlgorithm(bigGraph[getNumbPot(graph.cost[i])], i,OnlineSimulator.graph.adj(i));
            cost +=total_costs;
        }
        total_costs=cost;
        solGraph = solGraph.reverse();
        K_online=makespan(solGraph);
        result+="online makespan is " + K_online + System.getProperty("line.separator");
        result+="with reassignments: " + total_costs + System.getProperty("line.separator");
    }

    public final static long getNextPot(double w){
        long pot = 2;
        while (pot < w)
            pot *= 2;
        return pot;
    }

    public final static int getNumbPot(double w){
        long pot = 2;
        int numb = 1;
        while (pot < w) {
            pot *= 2;
            numb++;
        }
        return numb;
    }

    private static int matchingLoad(Graph G){
        int K = 0;
        for (int i=G.L(); i< G.V();i++){
            K = Math.max(G.degree(i), K);
        }
        return K;
    }

    private static double makespan(Graph G){
        double costs = 0;
        for (int i=G.L(); i< G.V();i++) costs = Math.max(G.makespan(i), costs);
        return (double)((long) (costs*10))/10;
    }

    private static double flowCosts(Graph G){
        double cost = 0;
        for (int i=G.L(); i< G.V();i++){
            for (Edge e: G.adj(i)){
                cost += e.weight();
            }
        }
        return cost;
    }

    private static double[] sortCosts(double[] array){
        double[][] numberWithIndex = new double[array.length][];
        for(int i = 0; i < array.length; i++) {
            numberWithIndex[i] = new double[] {array[i], i};
        }
        Arrays.sort(numberWithIndex, new Comparator<double[]>() {
            public int compare(double[] npi1, double[] npi2) {
                if (npi2[0] < npi1[0]) return -1;
                else if (npi2[0] > npi1[0]) return 1;
                else return 0;
            }
        });
        double[] costs = new double[array.length];
        for(int i=0;i<array.length;i++){
            costs[i]=numberWithIndex[i][1];
        }
        return costs;
    }

    /**
     * returns the cardinality L of bipartite Graph G=(L,R,E)
     *
     * @param G bipartite Graph
     * @return cardinality of L
     */
    public static int getL(Graph G) {
        int L = G.V();
        for (int i = 0; i < G.V(); i++) {
            for (Edge w : G.adj(i)) {
                L = Math.min(L, w.to());
            }
            if (L <= i) {
                L = i;
                break;
            }
        }
        for (int i = 0; i < L; i++) {
            for (Edge w : G.adj(i)) {
                if (w.to() < L)
                    throw new IllegalArgumentException("Graph has wrong format.");
            }
        }
        for (int i = L; i < G.V(); i++) {
            for (Edge w : G.adj(i)) {
                if (w.to() >= L)
                    throw new IllegalArgumentException("Graph has wrong format.");
            }
        }
        return L;
    }
}
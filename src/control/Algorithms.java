package control;

import model.Edge;
import model.Graph;

import java.util.NoSuchElementException;
import java.util.Random;

public class Algorithms {

    /**
     * @param G         flow network with arrival vertex t (sink)
     * @param t         node that arrive online
     * @return G        flow network with flow from source to sink t
     */
    public static Graph onlineFlowAlgorithm(Graph G, int t) {
        // search shortest path from source s to sink t
        BreadthFirstSearch bfs = new BreadthFirstSearch(G, G.V() - 1);
        OnlineSimulator.total_costs += bfs.distTo(t);
        // augment this path
        int latest = -1;
        for (int w : bfs.pathTo(t)) {
            if (latest < 0) {
                latest = w;
                continue;
            }
            G.flipEdge(G.getEdge(latest, w));
            latest = w;
        }
        return G;
    }

    /**
     * @param G         residual graph without v
     * @param v         node that we want insert into the matching
     * @param E         set of edges from v
     * @return G residual graph with v and one edge of v matched
     */
    public static Graph offlineMatchingAlgorithm(Graph G, int v, Iterable<Edge> E) {
        // update G^res
        for (Edge w : E) {
            G.addDirectedEdge(new Edge(w.from(), w.to(), w.weight()));
        }
        if (G.degree(v) == 0) return G;

        // search shortest path to node R with lowest outdegree
        BreadthFirstSearch bfs = new BreadthFirstSearch(G, v);
        int t = -1;
        double min = Integer.MAX_VALUE;
        for (int i = G.L(); i < G.V(); i++) {
            if (bfs.hasPathTo(i) && G.makespan(i) <= min) {
                min = G.makespan(i);
                t = i;
            }
        }
        if (t < 0) throw new NoSuchElementException("something is wrong: vertex should >= 0 but is " + v);

        // augment this path
        int latest = -1;
        for (int w : bfs.pathTo(t)) {
            if (latest < 0) {
                latest = w;
                continue;
            }
            G.flipEdge(G.getEdge(latest, w));
            latest = w;
        }
        return G;
    }

    /**
     * @param bigGraph    graph with big jobs with same exponent class without v
     * @param v         node (job) that arrive online
     * @param E         set of edges from v
     * @return bigGraph  bigGraph with v and one edge of v matched
     */
    public static Graph bigGraphAlgorithm(Graph bigGraph, int v,Iterable<Edge> E){
        int costs=0;
        Algorithms.onlineMatchingAlgorithm(bigGraph, v, E,(int)OnlineSimulator.getNextPot(OnlineSimulator.K));
        // deselect old edges and select new edges in our integer solution (flip)
        for(int j=OnlineSimulator.solGraph.L(); j<OnlineSimulator.solGraph.V();j++) {
            for (Edge e : bigGraph.adj(j)) {
                Edge e_old = OnlineSimulator.solGraph.getEdge(e.from(), e.to());
                if (e_old != null){
                    for (Edge flip_back : OnlineSimulator.solGraph.adj(e_old.to())) {
                        costs++;
                        OnlineSimulator.solGraph.flipEdge(flip_back);
                    }
                    OnlineSimulator.solGraph.flipEdge(e_old);
                }
            }
        }
        OnlineSimulator.total_costs=costs;
        return bigGraph;
    }

    /**
     * @param smallGraph    graph with small jobs without v
     * @param v         node (job) that arrive online
     * @param E         set of edges from v
     * @return smallGraph  smallGraph with v and one edge of v matched
     */
    public static Graph smallGraphAlgorithm(Graph smallGraph, int v,Iterable<Edge> E){
            double costs=0;
            Graph smallGraph_old = new Graph(smallGraph);
            //small job, calculate fractional problem
            // add edges of sink v in L as (*,v) with capacity infinity
            for (Edge w : E) smallGraph.addDirectedEdge(new Edge(w.to(), w.from(), 1, OnlineSimulator.getNextPot(OnlineSimulator.graph.cost[v])));
            //need to match cost[i] edges of job v, calc fractional problem - flow for sink v
            for (int j = 0; j <OnlineSimulator.getNextPot(OnlineSimulator.graph.cost[v]); j++) Algorithms.onlineFlowAlgorithm(smallGraph, v);
            // choose random a machine with correct property of new arrival job i
            double r = new Random().nextDouble();
            for (Edge e:smallGraph.adj(v)){
                r = r - (double) (e.capacity())/OnlineSimulator.getNextPot(OnlineSimulator.graph.cost[v]);
                if (r<=0) {
                    OnlineSimulator.solGraph.flipEdge(OnlineSimulator.solGraph.getEdge(e.to(), e.from()));
                    break;
                }
            }
            //MIN COST FLOW (RANDOM ROUNDING)
            for (int k=0;k<v;k++){
                if(smallGraph.degree(k)==0) continue;
                // flow, create L u R + source
                Graph min_cost_flow = new Graph(OnlineSimulator.graph.V()-OnlineSimulator.graph.L(),OnlineSimulator.graph.V()-OnlineSimulator.graph.L()+1,0);
                // create min cost flow
                for(Edge e2: smallGraph.adj(k)){
                    min_cost_flow.addDirectedEdge(new Edge(min_cost_flow.V() - 1, min_cost_flow.L() + e2.to() - OnlineSimulator.graph.L(), 0, e2.capacity()));
                    for(Edge e1: smallGraph_old.adj(k)){
                        if (e1.to() == e2.to())
                            min_cost_flow.addDirectedEdge(new Edge(min_cost_flow.L() + e2.to() - OnlineSimulator.graph.L(),e1.to()-OnlineSimulator.graph.L(),0,e1.capacity()));
                        else
                            min_cost_flow.addDirectedEdge(new Edge(min_cost_flow.L() + e2.to() - OnlineSimulator.graph.L(),e1.to()-OnlineSimulator.graph.L(),1,e1.capacity()));
                        min_cost_flow.cost[e1.to()-OnlineSimulator.graph.L()]= e1.capacity();
                    }
                }
                //calc min flow
                for (int m=0;m<min_cost_flow.L();m++){
                    for (int m_c =0; m_c<min_cost_flow.cost[m];m_c++) {
                        Algorithms.onlineFlowAlgorithm(min_cost_flow, m);
                    }
                }
                //get machine where we matched job v before
                Edge edge_old = OnlineSimulator.solGraph.adj(k).iterator().next();

                // calc new matching edge for k from min flow algo
                Edge e_new=null;
                Random rnd = new Random();
                int pot = 0;
                for(Edge e:min_cost_flow.adj(edge_old.to()-OnlineSimulator.solGraph.L())) pot += e.capacity();

                pot = rnd.nextInt((int)OnlineSimulator.getNextPot(OnlineSimulator.graph.cost[v]));
                for (Edge e:min_cost_flow.adj(edge_old.to()-OnlineSimulator.solGraph.L())){
                    pot = pot - (int)e.capacity();
                    if(pot <= 0) {
                        e_new = OnlineSimulator.solGraph.getEdge(min_cost_flow.L() - e.to() + OnlineSimulator.graph.L(),k);
                        break;
                    }
                }
                if(e_new!=null){
                    costs++;
                    OnlineSimulator.solGraph.flipEdge(edge_old);
                    OnlineSimulator.solGraph.flipEdge(e_new);
                }
            }
        OnlineSimulator.total_costs = costs;
        return smallGraph;
    }

    /**
     * @param G         residual graph without v
     * @param v         node that arrive online
     * @param E         set of edges from v
     * @param saturated definition which nodes we call saturate
     * @return G residual graph with v and one edge of v matched
     */
    public static Graph onlineMatchingAlgorithm(Graph G, int v, Iterable<Edge> E, int saturated) {
        // update G^res
        for (Edge w : E) {
            if (w.from() != v)
                throw new IllegalArgumentException("format in G is corrupt: Edge " + w + " in vertex " + v);
            G.addDirectedEdge(new Edge(w.from(), w.to()));
        }
        if (G.degree(v) == 0) return G;

        // search shortest path to unsaturated node in R
        BreadthFirstSearch bfs = new BreadthFirstSearch(G, v);
        int t = -1;
        double min = G.V() + 1;
        for (int i = G.L(); i < G.V(); i++) {
            if (G.degree(i) < saturated && bfs.hasPathTo(i) && bfs.distTo(i) <= min) {
                try {
                    if (bfs.distTo(i) == min && G.degree(t) < G.degree(i)) continue;
                } catch (IndexOutOfBoundsException ex) { ex.printStackTrace();
                }
                min = bfs.distTo(i);
                t = i;
            }
        }
        if (t < 0) throw new NoSuchElementException("No augmenting path found with degree <=" + saturated);

        // augment this path
        int latest = -1;
        int flips = 0;
        for (int w : bfs.pathTo(t)) {
            if (latest < 0) {
                latest = w;
                continue;
            }
            G.flipEdge(G.getEdge(latest, w));
            latest = w;
            flips++;
        }
        OnlineSimulator.total_costs += (flips - 1) / 2;
        return G;
    }
}
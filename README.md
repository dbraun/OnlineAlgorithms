# OnlineAlgorithms
Contains online algorithms for matchings, machine sheduling and flows.

## How To Execute Program:
Click on the right side on download zip, extract this zip and execute the JAR file.

In the 'test' folder are already example graphs.

## How To run GUI from Sourcecode:
Import the java project in IntelliJ. To compile and run the visual views, IntelliJ is necessary.

Execute 'GUIOnlineAlgorithms' and 'GUIGraphGenerator' from the view package.